<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="vouchers")
 * @ORM\Entity
 */
class Voucher
{
    const TYPE_VALUE = 'value';
    const TYPE_PERCENTAGE = 'percentage';

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", length=10)
     */
    private $discount_type;

    /**
     *
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $value;


    /**
     *
     * @ORM\Column(type="string", length=10)
     */
    private $product_type;

    /**
     *
     * @ORM\Column(type="integer")
     */
    private $product_ordinal = 0;

    /**
     *
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $min_amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getDiscountType(): ?string
    {
        return $this->discount_type;
    }

    public function setDiscountType(string $discount_type): self
    {
        $this->discount_type = $discount_type;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getProductType(): ?string
    {
        return $this->product_type;
    }

    public function setProductType(?string $product_type): self
    {
        $this->product_type = $product_type;

        return $this;
    }

    public function getProductOrdinal(): ?int
    {
        return $this->product_ordinal;
    }

    public function setProductOrdinal(?int $product_ordinal): self
    {
        $this->product_ordinal = $product_ordinal;

        return $this;
    }

    public function getMinAmount(): ?string
    {
        return $this->min_amount;
    }

    public function setMinAmount(?string $min_amount): self
    {
        $this->min_amount = $min_amount;

        return $this;
    }
}