<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="cart_vouchers")
 * @ORM\Entity
 */
class CartVoucher
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cart", inversedBy="items")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
     */
    private $cart;

    /**
     * @ORM\ManyToOne(targetEntity="Voucher")
     * @ORM\JoinColumn(name="voucher_id", referencedColumnName="id")
     */
    private $voucher;

    /**
     *
     * @ORM\Column(type="string", length=10)
     */
    private $discount_type;

    /**
     *
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $value;


    /**
     *
     * @ORM\Column(type="string", length=10)
     */
    private $product_type;

    /**
     *
     * @ORM\Column(type="integer")
     */
    private $product_ordinal = 0;

    /**
     *
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $min_amount;

    /**
     *
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $discount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getDiscountType(): ?string
    {
        return $this->discount_type;
    }

    public function setDiscountType(string $discount_type): self
    {
        $this->discount_type = $discount_type;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getProductType(): ?string
    {
        return $this->product_type;
    }

    public function setProductType(?string $product_type): self
    {
        $this->product_type = $product_type;

        return $this;
    }

    public function getProductOrdinal(): ?int
    {
        return $this->product_ordinal;
    }

    public function setProductOrdinal(?int $product_ordinal): self
    {
        $this->product_ordinal = $product_ordinal;

        return $this;
    }

    public function getMinAmount(): ?string
    {
        return $this->min_amount;
    }

    public function setMinAmount(?string $min_amount): self
    {
        $this->min_amount = $min_amount;

        return $this;
    }

    public function getVoucher(): ?Voucher
    {
        return $this->voucher;
    }

    public function setVoucher(?Voucher $voucher): self
    {
        $this->voucher = $voucher;

        return $this;
    }

    public function getCart(): ?Cart
    {
        return $this->cart;
    }

    public function setCart(?Cart $cart): self
    {
        $this->cart = $cart;

        return $this;
    }

    public function getDiscount(): ?string
    {
        return $this->discount;
    }

    public function setDiscount(string $discount): self
    {
        $this->discount = $discount;

        return $this;
    }
}