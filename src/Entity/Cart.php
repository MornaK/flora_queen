<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Carts")
 * @ORM\Entity
 */
class Cart
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="integer")
     */
    private $user_id;

    /**
     * @ORM\OneToMany(targetEntity="CartItems", mappedBy="cart")
     */
    private $items;

    /**
     * @ORM\OneToMany(targetEntity="CartVoucher", mappedBy="cart")
     */
    private $vouchers;

    /**
     *
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $amount;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->vouchers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return Collection|CartItems[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(CartItems $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setCart($this);
        }

        return $this;
    }

    public function removeItem(CartItems $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getCart() === $this) {
                $item->setCart(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CartVoucher[]
     */
    public function getVouchers(): Collection
    {
        return $this->vouchers;
    }

    public function addVoucher(CartVoucher $voucher): self
    {
        if (!$this->vouchers->contains($voucher)) {
            $this->vouchers[] = $voucher;
            $voucher->setCart($this);
        }

        return $this;
    }

    public function removeVoucher(CartVoucher $voucher): self
    {
        if ($this->vouchers->contains($voucher)) {
            $this->vouchers->removeElement($voucher);
            // set the owning side to null (unless already changed)
            if ($voucher->getCart() === $this) {
                $voucher->setCart(null);
            }
        }

        return $this;
    }
}