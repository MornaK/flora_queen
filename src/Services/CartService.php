<?php


namespace App\Services;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\CartVoucher;
use App\Entity\Product;
use App\Entity\Voucher;
use Doctrine\ORM\EntityManagerInterface;

class CartService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Cart
     */
    private $cart;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function createCart()
    {
        $this->cart = new Cart();
    }

    public function getCart() : Cart
    {
        return $this->cart;
    }

    public function addProduct(Product $product, int $quantity = 1)
    {
        // Can be optimized
        $isNew = true;
        foreach ($this->cart->getItems() as $cartItem) {
            if ($product->getId() == $cartItem->getProduct()->getId()) {
                $cartItem->setQuantity($cartItem->getQuantity() + $quantity);
                // Persist and stuff
                $isNew = false;
                break;
            }
        }

        if ($isNew) {
            $cartItem = new CartItem();
            $cartItem
                ->setProduct($product)
                ->setCart($this->cart)
                ->setPrice($product->getPrice())
                ->setQuantity($quantity)
            ;

            $this->cart->getItems()->add($cartItem);
        }

        $this->updateAmount();
    }

    public function removeProduct(Product $product, int $quantity = 1)
    {
        // Can be optimized
        foreach ($this->cart->getItems() as $cartItem) {
            if ($product->getId() == $cartItem->getProduct()->getId()) {
                if ($cartItem->getQuantity() > $quantity) {
                    $cartItem->setQuantity($cartItem->getQuantity() - $quantity);
                    // Persist and stuff
                }
                else {
                    $this->cart->getItems()->removeElement($cartItem);
                    // Persist and stuff
                }
                break;
            }
        }

        $this->updateAmount();

        // Handle error (not found product)
    }

    public function addVoucher(Voucher $voucher)
    {
        // Can be optimized
        $isNew = true;
        foreach ($this->cart->getVouchers() as $cartVoucher) {
            if ($voucher->getId() == $cartVoucher->getVoucher()->getId()) {
                // Handle duplicate (error or update voucher?)
                $isNew = false;
                break;
            }
        }

        if ($isNew) {
            $cartVoucher = new CartVoucher();
            $cartVoucher
                ->setVoucher($voucher)
                ->setCart($this->cart)
                ->setDiscountType($voucher->getDiscountType())
                ->setValue($voucher->getValue())
                ->setProductType($voucher->getProductType())
                ->setProductOrdinal($voucher->getProductOrdinal())
                ->setMinAmount($voucher->getMinAmount())
            ;
            // copy all voucher values

            $this->cart->getVouchers()->add($cartVoucher);
        }

        $this->updateAmount();
    }

    public function removeVoucher(Voucher $voucher)
    {
        // Can be optimized
        foreach ($this->cart->getVouchers() as $cartVoucher) {
            if ($voucher->getId() == $cartVoucher->getVoucher()->getId()) {
                $this->cart->getVouchers()->removeElement($cartVoucher);
                // Persist and stuff
                break;
            }
        }

        $this->updateAmount();

        // Handle error (not found voucher)
    }

    public function updateAmount()
    {
        $totalAmount = 0;
        // Update product lines
        foreach ($this->cart->getItems() as $cartItem) {
            $amount = $cartItem->getPrice() * $cartItem->getQuantity();
            $cartItem->setAmount($amount);
            $totalAmount += $amount;
        }

        // Update discount lines applied to product lines
        foreach ($this->cart->getVouchers() as $cartVoucher) {

            $amount = 0;
            if ($cartVoucher->getProductType() !== null) {
                foreach ($this->cart->getItems() as $cartItem) {
                    if ($cartItem->getProduct()->getType() == $cartVoucher->getProductType()) {

                        $apply = true;
                        $partialAmount = $cartItem->getAmount();
                        // min_amount
                        // by product type! if no product_type, min_amount will apply to total cart
                        if ($cartVoucher->getMinAmount() > $cartItem->getAmount()) {
                            $apply = false;
                        }

                        // product ordinal
                        // if there is enough quantity it applies once ...
                        // it will be different if should apply to each n-th product
                        if ($cartVoucher->getProductOrdinal() > $cartItem->getQuantity()) {
                            $apply = false;
                        }
                        else {
                            $partialAmount = $cartItem->getPrice();
                        }


                        if ($apply) {
                            if ($cartVoucher->getDiscountType() == Voucher::TYPE_VALUE) {
                                $amount = $cartVoucher->getValue();
                            }
                            else {
                                $amount = $partialAmount * $cartVoucher->getValue() / 100;
                            }
                        }
                        $cartVoucher->setDiscount($amount);
                        $totalAmount -= $amount;
                        break;
                    }
                }
            }
        }

        // Update discount lines (applied to the whole cart)
        foreach ($this->cart->getVouchers() as $cartVoucher) {
            $amount = 0;
            if ($cartVoucher->getProductType() == null) {
                if ($cartVoucher->getMinAmount() < $totalAmount) {
                    if ($cartVoucher->getDiscountType() == Voucher::TYPE_VALUE) {
                        $amount = $cartVoucher->getValue();
                    }
                    else {
                        $amount = $totalAmount * $cartVoucher->getValue() / 100;
                    }
                }

                $cartVoucher->setDiscount($amount);
                $totalAmount -= $amount;
            }
        }

        $this->cart->setAmount($totalAmount);
        // persist and stuff
    }
}