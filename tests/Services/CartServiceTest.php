<?php

namespace App\Tests\Services;

use App\Entity\Product;
use App\Entity\Voucher;
use App\Services\CartService;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class CartServiceTest extends WebTestCase
{
    /** @var CartService */
    private $cartService;

    /** @var Product */
    private $productA;
    /** @var Product */
    private $productB;
    /** @var Product */
    private $productC;

    /** @var Voucher */
    private $voucherV;
    /** @var Voucher */
    private $voucherR;
    /** @var Voucher */
    private $voucherS;

    public function setUp() {
        self::bootKernel();
        $this->cartService = self::$kernel->getContainer()->get('app.services.cart');

        $this->productA = new Product();
        $this->productA
            ->setId(101)
            ->setType('A')
            ->setPrice(10);
        ;

        $this->productB = new Product();
        $this->productB
            ->setId(201)
            ->setType('B')
            ->setPrice(8);
        ;

        $this->productC = new Product();
        $this->productC
            ->setId(301)
            ->setType('C')
            ->setPrice(12);
        ;


        $this->voucherV = new Voucher();
        $this->voucherV
            ->setId(5001)
            ->setProductType('A')
            ->setProductOrdinal(2)
            ->setDiscountType(Voucher::TYPE_PERCENTAGE)
            ->setValue(10)
        ;

        $this->voucherR = new Voucher();
        $this->voucherR
            ->setId(5002)
            ->setProductType('B')
            ->setDiscountType(Voucher::TYPE_VALUE)
            ->setValue(5)
        ;

        $this->voucherS = new Voucher();
        $this->voucherS
            ->setId(5003)
            ->setMinAmount(40)
            ->setDiscountType(Voucher::TYPE_PERCENTAGE)
            ->setValue(5)
        ;
    }

    public function testCartExample1()
    {
        $this->cartService->createCart();

        $this->cartService->addProduct($this->productA);
        $this->cartService->addProduct($this->productC);
        $this->cartService->addVoucher($this->voucherS);

        $this->cartService->addProduct($this->productA);
        $this->cartService->addVoucher($this->voucherV);
        $this->cartService->addProduct($this->productB);

        $cart = $this->cartService->getCart();

        $this->assertEquals(39, $cart->getAmount());
    }

    public function testCartExample2()
    {
        $this->cartService->createCart();

        $this->cartService->addProduct($this->productA);
        $this->cartService->addVoucher($this->voucherS);
        $this->cartService->addProduct($this->productA);

        $this->cartService->addVoucher($this->voucherV);
        $this->cartService->addProduct($this->productB);
        $this->cartService->addVoucher($this->voucherR);

        $this->cartService->addProduct($this->productC);
        $this->cartService->addProduct($this->productC);
        $this->cartService->addProduct($this->productC);

        $cart = $this->cartService->getCart();

        $this->assertEquals(55.10, $cart->getAmount());
    }
}